\documentclass[a4paper]{article}

% use UTF8
\usepackage[utf8]{inputenc}

% Table Handling
\usepackage{tabularx}
\usepackage{longtable}
\usepackage[table]{xcolor}
\usepackage{spreadtab}
\usepackage{etoolbox}

% Hyperlinking
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

% Include Graphics (Images, etc)
\usepackage{graphicx}


\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\newcolumntype{Y}{>{\raggedright\arraybackslash}X}

\renewcommand{\arraystretch}{1.5}

\newcommand{\rating}[1]{
	\ifnumless{#1}{20 * 100} {Very low} {
		\ifnumless{#1}{40 * 100} {Low} {
			\ifnumless{#1}{60 * 100} {Medium} {
				\ifnumless{#1}{80 * 100} {High} {
					Very high
				}
           		}
		}
	}
}

\begin{document}

\begin{titlepage}
	\vspace*{\fill}
	\begin{center}
		\hrule
		\textsc{\Huge \bfseries Project Proposal}\\[2em]
		\hrule
		\textsc{\LARGE Final year project}\\[2em]
		\textsc{\Large University of Newcastle}
	\end{center}
	\vspace*{\fill}
\end{titlepage}

\newpage
	\tableofcontents
\newpage

\section{Version History} {
		\begin{tabular}{ | p{0.1\textwidth} | p{0.6\textwidth} | p{0.3\textwidth} |}
			\hline
			\textbf{Version} & \textbf{Changes} & \textbf{Author} \\
			\hline

			0.1 &
			Initial template and risk profile analysis &
			Monica Olejniczak \\
			\hline

			0.2 &
			Project details; project purpose and objectives &
			Ben Morton \\
			\hline
			
			0.3 &
			Critical success factors  &
			Ben Atkinson \\
			\hline
			
			0.4 &
			Project cost; post production costs; project dependencies &
			Liam Jones \\
			\hline
			
			0.5 &
			Project Assumptions  &
			Tom Bradshaw \\
			\hline
			
			0.6 &
			Project Constraints &
			Aaron Murnain \\
			\hline
			
			0.7 &
			Alternative solutions; project source of funding &
			Dylan Barnett, Julius Myszkowski \\
			\hline
			
			0.8 &
			Project Benefits &
			Zach van Camp \\
			\hline
			
			0.9 &
			Re-writing sections to improve document flow &
			Jonathon Hope \\
			\hline
			
			1.0 &
			Further consistency checking and formatting &
			Brendan Annable \\
			\hline
			
		\end{tabular}
}


\section{Project overview} {

	\subsection{Project details} {

		\begin{tabularx}{\textwidth}{|l|Y|}
			\hline
			\textbf{Project name} & Final Year Project 2015 \\ \hline
			\textbf{Date} & \today \\ \hline
			\textbf{Project sponsor} & University of Newcastle \\ \hline
			\textbf{Business owner} & University of Newcastle \\ \hline
			\textbf{Project oversight} & Dr. Shamus Smith \\ \hline
			\textbf{Project manager} & Jonathon Hope \\ \hline
			\textbf{Stakeholders} &
				\begin{itemize}
					\item University of Newcastle
					\item School of Engineering and Computer Science
					\item Dr. Shamus Smith
					\item School of Nursing and Midwifery
					\item Prof. Sally Chan
					\item Dr. Sharyn Hunter
					\item Dr. Amanda Wilson
				\end{itemize}
				\\ \hline
			\textbf{Category} & Major \\ \hline
			\textbf{Duration} & 1 year \\ \hline
		\end{tabularx}

	}

	\subsection{Project purpose and objectives} {

		The project aims to:

		\begin{itemize}
			\item provide an interactive and collaborative system for improving case-based learning.
			\item improve the learning experience of students by targeting their engagement with intutive user interfaces.
			\item utilise good software engineering practice wherever possible.
			\item inform the rationale for future university projects of a similar nature.
		\end{itemize}

	}

	\subsection{Project scope} {

		\paragraph{Inclusions} {

			A software system that:
			\begin{itemize}
				\item facilitates \emph{case-based learning} within the domain of \emph{Nursing}.
				\item utilises multi-touch hardware to provide interactivity within student excercises.
				\item enables student progress monitoring and provides this information to the teacher.
				\item is appropriate for a collaborative environment and supports multiple users.
			\end{itemize}
			
			Integration of existing content data from the previous system is to be included within the scope of this project.

		}

		\paragraph{Exclusions} {

			The following are excluded from the \emph{project scope}:
			\begin{itemize}
				\item communication with existing University Systems, including but not limited to UoNline Blackboard and University student databases.
				\item development of a full featured CMS.
				\item development of a system that functions on multiple platforms.
			\end{itemize}

		}
		
		There will be a full list of requirements, both functional and non-functional, published in a future document.

	}

}

\clearpage

\section{Project justification} {

	\subsection{Project benefits} {

		The following section outlines the main benefits that the project provides to the client.

		\subsubsection{Teacher feedback with user monitoring} {
			
			Teachers will be provided with instant feedback regarding their students' progress, empowering them to make quick and adaptive decisions to help guide their students appropriately.
			
		}

		\subsubsection{Greater student engagement} {
			
			Students will naturally become more engaged with the learning material due to the interactive and collaborative environment that the multi-touch tables provide \cite{evans2007interactivity}. A greater user engagement translates easily into quicker learning and a lasting experience that can be more-readily be memorized and understood.
			
		}
		
		\subsubsection{Easier content management} {

			Teachers were unable to easily manage the content of the cases that the students will study. This project provides the opportunity for a Content Management System (CMS) to be developed, in collaboration with the client, to provide a streamlined process for teachers to easily update and maintain the content on the system.
			
			This project also provides the opportunity to build a workflow management system, where the teachers can build 'unlockable' learning modules into the system, again providing direct and instant feedback that the students are learning.

		}
	}

	\subsection{Critical success factors} {
	
		The critical success factors for the project can be organised into three categories:
		
		\begin{enumerate}
			\item People-related factors			\item Process-related factors 
			\item Technical-related factors
		\end{enumerate}

		Concordantly, the projects critical success factors have been placed into these categories:

		\paragraph{People-related factors:} {
				\begin{itemize}
					\item Client involvement  
					\item Effective communication and feedback
				\end{itemize}
		}
		
				These factors are predominately produced by the interactions of people, escpe- cially stakeholders and the development team.		In the case of this project, involvement of the client continuously is fitting with an agile-methodology because it allows the client to make decisions gradually and dynamically as the product is refined and helps avoid developing a product that does not meet expectations. On the other hand, effective communication is required to ellucidate requirements from the client and help both sides exchange feedback.
				These are being addressed by approaching the client interactions from a non- technical perspective and also by selecting people that are suited to task of liaising with the client. The internal communication is also important and this is being handled both by our software tooling: Atlassian HipChat and Confluence help us communicate across both casual and formal methods.
		
		
		\paragraph{Process-related factors:} {
				\begin{itemize}
					\item Appropriate development processes
					\item Adequate resources 
					\item Clear requirements
					\item Clear objectives
					\item Realistic schedule
				\end{itemize}
		}
		
		These factors stem from processes: either those mandated by the university or those chosen by the development team. Most of the critical success factors outlined in this section are categorically process-related.		
		Software projects are equally about utilising appropriate development processes as they are about technical understanding of a particular library or piece of code. They require standardisation on these processes in order to execute development in a consistent manner. This factor is being balanced by using industry standard software development tools and researching into the best practices of those tools; we have agreed on the resultant processes.		
		Adequate resourcing is necessary to ensure the development team has access to the tools that aid them in integrating developmental concerns. As an example, soliciting a server for installing our development infrastructure was a matter of resourcing. Addressing this was about engagement with the University process for requesting an IT-resource.

		Clear requirements and clear objectives are along the same lines. They both represent the fruition of successful requirements gathering and honing in on the most important. The plan to address this factor is to have an iterative process where requirements are gathered from the highest level to the lowest level; the client will have many opportunities to clarify their requirements.	
		Setting realistic goals about balancing cognitive-biases with expectations and having the perspicacity to determine which strategies are pragmatic and which are likely idealistic. Realistic scheduling therefore is likely to have a profound impact on the project, because it will allow us to realise the project aims within the finite amount of time.
		\paragraph{Technical-related factors:} {
				\begin{itemize}
					\item Learning the chosen framework / platform
					\item Supporting tools and good infrastructure
				\end{itemize}
		}
		
		These factors arise from technical issues. Understanding a the SynergyNet framework presents a huge technical challenge and may be significant enough to warrant pursuing alternatives. We are dealing with these factors by reverse engineering and offsetting the risks by searching for alternatives.
				As for the supporting tools and infrastructure, we have very good tools but the deployment of them on the University is less than ideal. There is current research underway to pursue a way of addressing the server outages.
		
		\paragraph{Deliverables} {
		
			The project plan and schedule is built around the existing dates for our deliverables. Please see the project Gantt Chart for our schedule.
				
			\begin{tabular}{| p{0.4\textwidth} | p{0.5\textwidth} |}
				\hline
				\textbf{Deliverable} & \textbf{Due Date}  \\ \hline
				\emph{Project Proposal} & 16/03/15 \\  \hline
				\emph{Requirements Document} & 27/03/15 \\  \hline
				\emph{Design Document} & 27/05/15 \\ \hline
				\emph{Prototype} & 27/05/15 \\ \hline
			\end{tabular}
		}
	
	}

    \subsection{Determining success factors} {
	Because the project is non-commercial, its revenue generation and cost saving may be considered irrelevant from a business perspective. As a consequence, other forms for measuring success will be established (refer to next section).

	When considering and prioritising success criteria, it becomes necessary to relate the success factors to the domain of the project (software engineering). Measures of success pertaining specifically to software systems such as this include, but not limited to the following:

	        \begin{itemize}
		        	\item Responsiveness of end system
		        	\item Robustness and modularity of code
		        	\item Reliability and stability of system
		        	\item Efficient memory and CPU utilisation
		        	\item Security, particularly for administrator portal
		        	\item Maintainable architecture
		        	\item Cleanliness and intuitive user interface
		        	\item Modifiable content provided by an easy-to-use CMS (content management system)
		        	\item Fault detection and fault tolerance
	        	\end{itemize}
	}

	\subsection{Alternative solutions} {

		\subsubsection{Traditional approach to teaching} {

			Rather than using the technology the teacher could present the materials themselves in a classroom environment. This would involve handing out the materials and displaying videos to the entire group. The student could then break off into the groups to discuss the material presented.

			This approach is more traditional of a classroom environment however the teacher then has to take the time to present the materials leaving less time for the discussion. It also takes away from the hands on experience a technology based system would provide. We will not be using the approach as the aim of the system is to create a hands on collaborative learning experience.

		}

		\subsubsection{Buying an existing product} {

			There are existing products for multi-touch environments and software designed to use these in a learning environment such as tablerTV, Ideum or activTable. These systems could be used as is or modified slightly to facilitate the case based learning system.

			While some of the buying options offer superior technology for a lower price it would not be feasible to buy any of these systems as we already have the hardware resources to build on and we don't have the budget capabilities to buy any others. Any alternative purchases would still require software to be built on top of the packaged operating system.

		}

		\subsubsection{Bring your own device} {

			The students could bring their own smart-phones or tablets and log in to an application that allows the viewing of content and sharing of information between devices. This would allow for easier monitoring of individual contributions and actions as each user would have their own device.

			This option was discussed in depth but was rejected as it did not easily allow for the collaboration of a group. The students could share the resources but group discussions about an individual item would be more difficult. This system would also require all students to have a device and development of an application across different platforms.

		}

		\subsubsection{Microsoft Kinect integration} {

			Using kinects that are available another option would be a visual gesture recognition system that tracks a user allowing them to display and manipulate information on a central screen without the need to be standing at it. This would allow a teacher to display documents and focus on specific details while still going around to each of the groups and monitoring the learning process.

			This solution was considered unfeasible due to the difficulties of setting up the system in a way that would reduce feedback while still having full room coverage. Without this the setup could have dead zones in room where no gestures would be recognised or zones where they would be seen and used twice.

		}

	}

}

\clearpage

\section{Project details} {

	\subsection{Project timeline and resources} {

		The project timeline is detailed in the \emph{Gantt Chart} that was submitted alongside this document (please see FYP-Gantt-Chart.pdf). 
		
		\paragraph{Waterfall Approach} {
			In a typical \emph{waterfall} project methodology, the following take place in a rigid sequence:
			
			\begin{enumerate}
				\item Planning and requirements
				\item Designing and modelling
				\item Implementation, 
				\item Quality assurance and testing 
				\item Product deployment		
			\end{enumerate}
		}
		
		\paragraph{Agile Approach} { 
			The plan aligns more with an \emph{Agile-Scrum} project management methodology, which partitions the total project timeline into segmented iterations of planning and work, called \emph{sprints}. These sprints each consist of the following sequence:
			\begin{enumerate}
				\item Sprint Planning
				\item Implementation, 
				\item Testing 
			\end{enumerate}
			and conclude with a release that can be shown to the client for criticism.
			
			During the \emph{sprint planning} phase of each sprint, a team meeting is held in which the high level goals for the current iteration are decomposed into more specific issues that are subsequently assigned. 
			
			The project sprints for the first half of the project are outlined below.
	
			\begin{tabularx}{\textwidth}{|c|Y|Y|Y|}
				\hline
				\textbf{Sprint} & \textbf{Estimated duration (weeks)} & \textbf{Estimated completion date} & \textbf{Goal} \\ \hline
				1 & 1 & 8/03/15 & Development ecosystem setup and processes \\ \hline
				2 & 1 & 16/03/15 & \emph{Project Plan Document} \\ \hline
				3 & 2 & 27/03/15 & Requirements Gathering, \emph{Requirements Document} \\ \hline
				4 & 2 & 10/04/15 & Architecture and technology stack \\ \hline
				5 & 2 & 24/04/15 & Interface design \\ \hline
				6 & 2 & 08/05/15 & Prototype part 1 \\ \hline
				7 & 2 & 27/05/15 & Prototype part 2 and \emph{Design Document} \\ \hline
				8 & 1 & 05/06/15 & Finish interim reports \\ \hline
			\end{tabularx}
			
			\emph{Please note} that the first two sprints detailed in the table, have been completed and you can view a more detailed snapshot of sprint planning on public \href{https://trello.com/b/5YjAWCCo/fyp}{Trello board}.
			
			\paragraph{Issue management} {
			
				In the first sprint, the table alludes to \emph{development ecosystem} which is our approach to issue tracking, code review and other integration software. The project will use \emph{Atlassian JIRA} is used for all issue tracking and several related development products that are listed in the dependencies. Figure~\ref{fig:workflow} shows our workflow for resolving each issue.  
			}
			
			\begin{figure}[p]
				\centering
				\includegraphics[width=13cm]{Workflow.png} 
				\caption{Our JIRA Workflow}
				\label{fig:workflow}
			\end{figure}	

		}

	}

	\subsection{Project dependencies} {

		\begin{tabularx}{\textwidth}{|Y|Y|}
			\hline
			\textbf{Project/System/Third party} & \textbf{Dependency} \\ \hline
			Jira & Issue tracking \\
			Confluence & Collaboration \\
			BitBucket & Source hosting \\
			HipChat & Communication \\
			Stash & Git repository \\
			FishEye & Revision control \\
			SourceTree & Git/Mercurial client \\
			Crucible & Code review \\
			Bamboo & Continuous integration server \\
			Glassfish & Server \\
			LaTeX & Typesetting (documentation only) \\
			SynergyNet & Multi-touch software \\ \hline
			Multi-touch tables & Hardware \\ \hline
			Shamus Smith & Project overseer \\
			Chan, Hunter \& Wilson & Clients \\ \hline
		\end{tabularx}

	}

	\subsection{Project constraints} {

		\subsubsection{Time} {

			This project has a strict, non-negotiable deadline for both the prototype and final delivery. These deliverables facilitate the need for excellent project management and its progress must be constantly monitored to ensure that the project can be completed successfully.

			Developers have a strictly limited number of hours per week to contribute to the project, and as such, this limitation must be considered when assigning tasks. The overloading of developers will have a direct negative impact on the project progress and quality of the product.

		}

		\subsubsection{Budget} {

			There are no allocated monetary resources for this project. The procurement of additional resources will need to either be negotiated with the University of Newcastle, or paid for by members of the team.

		}

		\subsubsection{Technical} {

			Each team member offers varying levels of technical expertise to the project and could result in the need for extensive training. This training aims to teach members the required set of skills that will be utilised throughout the development of the project; it has the potential to impact the time needed to work on the project significantly. However, this will become less relevent as the project continues and members grow accustomed to what is required of the project.

		}

		\subsubsection{Resources} {

			A shared key is required to unlock the research laboratory where the touch tables are stored. This poses as a major inconvenience to the team as the room is not always accessible. There are also a limited amount of touch tables and not all are currently operational, which greatly limits the team's ability to test modules.

			The software and hardware that powers the touch tables is outdated, and without an allocated budget many components cannot be upgraded. This restricts how the team develops the final product, since the tables can not be used to their full potential.

		}

	}

	\subsection{Assumptions} {

		For the planning of this project a number of assumptions have been made.

		\subsubsection{Team size} {

			We assume that the size of the team shall remain constant. We currently have 13 members.

		}

		\subsubsection{Delivery date} {

			We assume that we have a very hard deadline due to this project being conducted as a university assessment.

		}

		\subsubsection{Team member availability} {

			We assume that all members of the team will be able to allocate at least 10 hours per week to the project.

		}

		\subsubsection{Tooling} {

			We assume that we will have the ability to consistently access and make use of all operational tooling that we decide to use. This includes things such as our issue tracker and code repository.

		}

		\subsubsection{Stakeholders} {

			We assume that the core group of stakeholders shall remain constant.

		}

		\subsubsection{Domain knowledge} {

			We assume that the subject matter experts will be available within reasonable limits. We also assume that any user consumed content from the system will be provided by either the subject matter experts or other stakeholders.

		}

	}

	\subsection{Project funding} {

		There is no funding for this project.

	}

	\subsection{Project cost} {

		Almost all hardware and software the project requires are available at no cost, consequently budget issues are not of great concern to the project. SynergyNet, Glassfish and LaTeX are all available free of charge, while all products, namely those from Atlassian's suite are being used on an educational license.
		
		\subsubsection{Domain name registration} {
			\textbf{\$13/year} - Cost for the domain name uonse.org. This domain is used to host our internal web-based project management software, including JIRA, Confluence, Crucible/Fisheye.
		}
		
		\subsubsection{VPS server} {
			\textbf{\$5/month} - Cost of a VPS droplet hosted by DigitalOcean. This is required to allow access to our project management software on the public internet, significantly improving their accessibility and use within the development team.
		}
		
		\subsubsection{Developer time} {
			\textbf{100 hours/week} - The main cost of this project is time, with 100/hours a week dedicated to the project by the entire team. With 13 current members, this equates to approximately 7 hours per person per week.
		}

	}

	\subsection{On-going costs post production} {

		It should be emphasised that the following are estimates produced at a very early stage in development, and may therefore change, or be extended.

		\begin{tabularx}{\textwidth}{|l|l|l|Y|}
			\hline
			\textbf{Cost type} & \textbf{Hew level} & \textbf{Type} & \textbf{Cost per annum} \\ \hline
% What licenses are being used?
%			Resources & 1 & New & COST \\ \hline
			IT support & 3 & New & 0 or HEW 3 salary (see below) \\ \hline
%			License \& support & 1 & New & COST \\ \hline
%			Ongoing license fee & 1 & Existing & COST \\ \hline
			Maintenance and support\textsuperscript{[1]} & 5 & Existing & \shortstack[l]{IR camera (4): \$150 \\ Glass: \$12000 \\ Hardware: \$750} \\ \hline
		\end{tabularx}

		The cost of IT support depends on whether an employee must be hired, either to provide dedicated support, or as a member of a team providing support to a range of products. In this case the cost is the salary of a HEW 3 employee. If the support can be added to an employee's responsibilities without hiring additional personnel then the cost is zero.
		\medskip

		\textsuperscript{[1]} Since the specific model (and therefore cost) of each component, as well as the rate of replacement, is not known, we present the estimated cost of replacing each component of a single table, not the estimated annual maintenance cost per table. It is almost certain that not all components will require replacement at the same time, so the actual cost of maintenance will be much less than the values presented. In addition there is the time cost (not shown) of re-calibrating the table (especially after actions relating to the cameras); this value is large but unknown.

	}

	\subsection{Project risk profile analysis} {

		\subsubsection{Category legend} {

			\begin{tabularx}{\textwidth}{|l|Y|}
				\hline
				\textbf{Risk category} & \textbf{Examples} \\ \hline
				Project management & Operational, organisational and contractual software development parameters. \\ \hline
				Process management & Planning, staffing, tracking, quality assurance and configuration management. \\ \hline
				Technical process & Analysis, design, programming and testing. \\ \hline
				Technical product & Requirements stability, design performance, code complexity and test specifications. \\ \hline
			\end{tabularx}

		}

		\subsubsection{Strategy legend} {

			\begin{tabularx}{\textwidth}{|l|Y|}
				\hline
				\textbf{Risk strategy} & \textbf{Description} \\ \hline
				Acceptance & The risk is acceptable and will be account for. \\ \hline
				Avoidance & An activity will not be performed if it may result in a risk. \\ \hline
				Reduction & The severity of the impact or the likelihood of the risk will be reduced. \\ \hline
				Research & The risk will be investigated so it can be managed. \\ \hline
				Transfer & The risk is shifted to or outsourced to another person, group or organisation. \\ \hline
			\end{tabularx}

		}

		\subsubsection{Risk rating legend} {

			\begin{tabularx}{\textwidth}{|Y|Y|}
				\hline
				\textbf{Priority score} & \textbf{Risk rating} \\ \hline
				0 - 19 & Very low \\ \hline
				20 - 39 & Low \\ \hline
				40 - 59 & Medium \\ \hline
				60 - 79 & High \\ \hline
				80 - 100 & Very high \\ \hline
			\end{tabularx}

		}

		\subsubsection{Risks} {

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ The project is not satisfactorily completed by the deadline. \\ \hline
				@ \textbf{Category} 	& @ Project management \\ \hline
				@ \textbf{Likelihood} 	& :={35}\%  \\ \hline
				@ \textbf{Impact} 		& 100 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @
					\begin{itemize}
						\item Ensure that milestones are met in accordance with the project timeline.
						\item Ensure there are processes in place for:
							\begin{itemize}
								\item Balancing the workload amongst the group by fairly distributing tasks.
								\item Tracking and logging the amount of work contributed by each member.
								\item Handling members that do not contribute to the workload.
							\end{itemize}
					\end{itemize} \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ The requirements of the project are misinterpreted or not fully communicated. \\ \hline
				@ \textbf{Category} 	& @ Project management \\ \hline
				@ \textbf{Likelihood} 	& :={5}\%  \\ \hline
				@ \textbf{Impact} 		& 80 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ The requirements for the system will constantly be clarified and revised with each client meeting. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Tasks are not distributed evenly amongst members. \\ \hline
				@ \textbf{Category} 	& @ Project management \\ \hline
				@ \textbf{Likelihood} 	& :={5}\%  \\ \hline
				@ \textbf{Impact} 		& 85 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ Team leaders are responsible for assigning tasks fairly to their group. When a task has been assigned to a member that is more demanding than others, the rest of the team can provide assistance with the task. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Team members with limited technical expertise slow the development of the project. \\ \hline
				@ \textbf{Category} 	& @ Technical process \\ \hline
				@ \textbf{Likelihood} 	& :={5}\%  \\ \hline
				@ \textbf{Impact} 		& 85 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Avoidance \\ \hline
				@ \textbf{Action plan} 	& @ Team leaders should assign tasks that are within the skill level of the assigned member. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ The server provided by the University becomes unavailable. \\ \hline
				@ \textbf{Category} 	& @ Technical process \\ \hline
				@ \textbf{Likelihood} 	& :={50}\%  \\ \hline
				@ \textbf{Impact} 		& 100 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Acceptance \\ \hline
				@ \textbf{Action plan} 	& @ This risk is out of the team's control but can be resolved by contacting the server manager. The information stored on the server is also backed up regularly by the University's IT department. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Members of the project do not complete their assigned task. \\ \hline
				@ \textbf{Category} 	& @ Process management \\ \hline
				@ \textbf{Likelihood} 	& :={30}\%  \\ \hline
				@ \textbf{Impact} 		& 75 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ Team leaders are responsible for keeping track of their team's overall progress and reporting their progress to the Project Manager. All hours that contribute to the project will be logged, used as a tool for determining which members are not contributing fairly to the workload and will be held accountable. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Development time is slow with the use of SynergyNet since the framework is unmaintained, undocumented, untested and relies on patched dependencies. \\ \hline
				@ \textbf{Category} 	& @ Technical process \\ \hline
				@ \textbf{Likelihood} 	& :={90}\%  \\ \hline
				@ \textbf{Impact} 		& 75 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ This framework needs to be learned quickly to minimise its impact on the group. The more people that are familiar with the framework, the easier it will be for others to learn as they are able to impart their knowledge. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Hardware failure slows or prevents development. \\ \hline
				@ \textbf{Category} 	& @ Technical product \\ \hline
				@ \textbf{Likelihood} 	& :={10}\%  \\ \hline
				@ \textbf{Impact} 		& 85 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ All team members should be gentle when handling their own hardware and the touch tables. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ The scope of the project becomes too large. \\ \hline
				@ \textbf{Category} 	& @ Process management \\ \hline
				@ \textbf{Likelihood} 	& :={30}\%  \\ \hline
				@ \textbf{Impact} 		& 75 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ The core requirements of the system should be extracted from the client and refined during client meetings. The team will need to keep in regular touch with the client to ensure they are aware of what is technically feasible and negotiate other requirements. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Code is lost. \\ \hline
				@ \textbf{Category} 	& @ Technical process \\ \hline
				@ \textbf{Likelihood} 	& :={5}\%  \\ \hline
				@ \textbf{Impact} 		& 80 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ Git provides a high level of redundancy reducing the risk of code being lost. Commits should be kept small to ensure that in the event of loss, it is of minimal impact. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Too many processes slow development. \\ \hline
				@ \textbf{Category} 	& @ Process management \\ \hline
				@ \textbf{Likelihood} 	& :={75}\%  \\ \hline
				@ \textbf{Impact} 		& 90 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ When this becomes an issue, the amount of processes can be reduced so that members are using the most important one's that are in place, that also work for the majority of the group. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Code that breaks the system or otherwise does not work is committed to the repository. \\ \hline
				@ \textbf{Category} 	& @ Technical process \\ \hline
				@ \textbf{Likelihood} 	& :={5}\%  \\ \hline
				@ \textbf{Impact} 		& 50 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ Review processes are in place to prevent this from occurring. This process also requires unit tests to accompany the written code. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Processes are not followed. \\ \hline
				@ \textbf{Category} 	& @ Process management \\ \hline
				@ \textbf{Likelihood} 	& :={80}\%  \\ \hline
				@ \textbf{Impact} 		& 30 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ Training procedures will be put into place to minimise this issue. Those that do not correctly follow the processes will be made aware and notified in an attempt to correct it. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ Discussions are lost or forgotten. \\ \hline
				@ \textbf{Category} 	& @ Process management \\ \hline
				@ \textbf{Likelihood} 	& :={5}\%  \\ \hline
				@ \textbf{Impact} 		& 25 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ This is resolved by documenting all meetings and discussions through meeting minutes. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ The large groups size becomes unmanageable. \\ \hline
				@ \textbf{Category} 	& @ Process management \\ \hline
				@ \textbf{Likelihood} 	& :={5}\%  \\ \hline
				@ \textbf{Impact} 		& 80 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ The likelihood of this issue is lessened by splitting the group into subgroups, having documented processes in place and using the Atlasssian stack to manage software. \\ \hline
			\end{spreadtab}

			\begin{spreadtab}{{tabularx}{\textwidth}{|l|Y|}}
				\hline
				@ \textbf{Description} 	& @ The communication between members is unclear and not to the point. \\ \hline
				@ \textbf{Category} 	& @ Project management \\ \hline
				@ \textbf{Likelihood} 	& :={5}\%  \\ \hline
				@ \textbf{Impact} 		& 20 \\ \hline
				@ \textbf{Score} 		& (b3 / 100) * b4 \\ \hline
				@ \textbf{Rating} 		& \rating{:={b5 * 100}} \\ \hline
				@ \textbf{Strategy} 	& @ Reduction \\ \hline
				@ \textbf{Action plan} 	& @ Through the use of HipChat, communication will be more concise and less distracting than other technologies as it is designed for communicating within a team environment. \\ \hline
			\end{spreadtab}

		}

	}

}

\bibliographystyle{apalike}
\bibliography{project-proposal}

\end{document}
