\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{tabularx}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\newcolumntype{Y}{>{\raggedright\arraybackslash}X}

\begin{document}

\begin{titlepage}
    \vspace*{\fill}
    \begin{center}
    	\hrule
    	\textsc{\Huge \bfseries Project Proposal}\\[2em]
		\hrule
		\textsc{\LARGE Final year project}\\[2em]
        \textsc{\Large University of Newcastle}
    \end{center}
    \vspace*{\fill}
\end{titlepage}

\newpage
	\tableofcontents
\newpage

\section{Project overview} {

	\subsection{Project details} {

		\begin{tabularx}{\textwidth}{|l|Y|}
			\hline
			\textbf{Project name} & Final Year Project 2015 \\ \hline
			\textbf{Date} & \today \\ \hline
			\textbf{Project sponsor} & University of Newcastle \\ \hline
			\textbf{Business owner} & University of Newcastle \\ \hline
			\textbf{Project oversight} & Dr. Shamus Smith \\ \hline
			\textbf{Project manager} & Jonathon Hope \\ \hline
			\textbf{Stakeholders} &
				\begin{itemize}
					\item University of Newcastle
					\item School of Engineering and Computer Science
					\item Dr. Shamus Smith
					\item School of Nursing and Midwifery
					\item Prof. Sally Chan
					\item Dr. Sharyn Hunter
					\item Dr. Amanda Wilson
				\end{itemize}
				\\ \hline
			\textbf{Category} & Major \\ \hline
			\textbf{Duration} & 1 year \\ \hline
		\end{tabularx}

	}

	\subsection{Project purpose and objectives} {

		The project aims to:
	
		\begin{itemize}
			\item provide an interactive and collaborative system for improving case-based learning.
			\item improve the learning experience of students by targeting their engagement with intutive user interfaces.
			\item utilise good software engineering practice wherever possible.
			\item inform the rationale for future university projects of a similar nature.
		\end{itemize}
	
	}

	\subsection{Project scope} {

		\paragraph{Inclusions} {
	
				A software system that:
				\begin{itemize}
					\item facilitates \emph{case-based learning} within the domain of \emph{Nursing}.
					\item utilises multi-touch hardware to provide interactivity within student excercises.
					\item enables student progress monitoring and provides this information to the teacher.
					\item is appropriate for a collaborative environment and supports multiple users.
				\end{itemize}
				
				Integration of existing content data from the previous system is to be included within the scope of this project.
	
			}
	
			\paragraph{Exclusions} {
	
				The following are excluded from the \emph{project scope}:
				\begin{itemize}
					\item communication with existing University Systems, including but not limited to UoNline Blackboard and University student databases.
					\item development of a system that functions on multiple platforms.
				\end{itemize}
	
			}

	}

}

\clearpage

\section{Project justification} {

	\subsection{Project benefits} {
	
		The following section outlines the main benefits that the project provides to the client.
	
		\subsubsection{Teacher feedback with user monitoring} {
			
			Teachers will be provided with instant feedback regarding their students' progress, empowering them to make quick and adaptive decisions to help guide their students appropriately.
			
		}

		\subsubsection{Greater student engagement} {
			
			Students will naturally become more engaged with the learning material due to the interactive and collaborative environment that the multi-touch tables provide \cite{evans2007interactivity}. A greater user engagement translates easily into quicker learning and a lasting experience that can be more-readily be memorized and understood.
			
		}
		
		\subsubsection{Easier content management} {

			Teachers were unable to easily manage the content of the cases that the students will study. This project provides the opportunity for a Content Management System (CMS) to be developed, in collaboration with the client, to provide a streamlined process for teachers to easily update and maintain the content on the system.
			
			This project also provides the opportunity to build a workflow management system, where the teachers can build 'unlockable' learning modules into the system, again providing direct and instant feedback that the students are learning.

		}

	}

	\subsection{Critical success factors} {

		The following categorisation of factors that contribute to the successful delivery of the deliverables for this project are outlined below:

		\paragraph{People-related factors:} {
				\begin{itemize}
					\item Client involvement  
					\item Effective communication and feedback
				\end{itemize}
		}
		
		\paragraph{Process-related factors:} {
				\begin{itemize}
					\item Appropriate development processes
					\item Adequate resources 
					\item Clear requirements
					\item Clear objectives
					\item Realistic schedule
				\end{itemize}
		}

		\paragraph{Technical-related factors:} {
				\begin{itemize}
					\item Learning the chosen framework / platform
					\item Supporting tools and good infrastructure
				\end{itemize}
		}
		
		\paragraph{Deliverables} {
		
			The project plan and schedule is built around the existing dates for our deliverables. Please see the project Gantt Chart for our schedule.
				
			\begin{tabular}{| p{0.4\textwidth} | p{0.5\textwidth} |}
				\hline
				\textbf{Deliverable} & \textbf{Due Date}  \\ \hline
				\emph{Project Proposal} & 16/03/15 \\  \hline
				\emph{Requirements Document} & 27/03/15 \\  \hline
				\emph{Design Document} & 27/05/15 \\ \hline
				\emph{Prototype} & 27/05/15 \\ \hline
			\end{tabular}
		}

	}

    	\subsection{Project approach} {

		The overall project approach aligns with an \emph{Agile-Scrum} project management methodology, which partitions the total project timeline into segmented iterations of planning and work, called \emph{sprints}. These sprints each consist of the following sequence:
		\begin{enumerate}
			\item Sprint Planning
			\item Implementation, 
			\item Testing 
		\end{enumerate}
		and conclude with a release that can be shown to the client for criticism.
		
		During the \emph{sprint planning} phase of each sprint, a team meeting is held in which the high level goals for the current iteration are decomposed into more specific issues that are subsequently assigned. 

  	}

	\subsection{Alternative solutions} {

		Please refer to the Project Proposal document for more detail.

		\begin{itemize}
			\item Traditional approach to teaching
			\item Buying an existing product
			\item Bring your own device
			\item Microsoft Kinect integration
		\end{itemize}

	}

}

\clearpage
\section{Project details} {

	\subsection{Project timeline and resources} {

		The project timeline is detailed in the \emph{Gantt Chart} that was submitted alongside this document (please see FYP-Gantt-Chart.pdf). 

		The project sprints for our Agile-Scrum based approach are outlined below for the first half of the project .
	
		\begin{tabularx}{\textwidth}{|c|Y|Y|Y|}
			\hline
			\textbf{Sprint} & \textbf{Estimated duration (weeks)} & \textbf{Estimated completion date} & \textbf{Goal} \\ \hline
			1 & 1 & 8/03/15 & Development ecosystem setup and processes \\ \hline
			2 & 1 & 16/03/15 & \emph{Project Plan Document} \\ \hline
			3 & 2 & 27/03/15 & Requirements Gathering, \emph{Requirements Document} \\ \hline
			4 & 2 & 10/04/15 & Architecture and technology stack \\ \hline
			5 & 2 & 24/04/15 & Interface design \\ \hline
			6 & 2 & 08/05/15 & Prototype part 1 \\ \hline
			7 & 2 & 27/05/15 & Prototype part 2 and \emph{Design Document} \\ \hline
			8 & 1 & 05/06/15 & Finish interim reports \\ \hline
		\end{tabularx}

	}

	\subsection{Project dependencies} {

		\begin{tabularx}{\textwidth}{|Y|Y|}
			\hline
			\textbf{Project/System/Third party} & \textbf{Dependency} \\ \hline
			Jira & Issue tracking \\
			Confluence & Collaboration \\
			BitBucket & Source hosting \\
			HipChat & Informal Communication \\
			Stash & Git repository \\
			FishEye & Revision control \\
			SourceTree & Git/Mercurial client \\
			Crucible & Code review \\
			Bamboo & Continuous integration server \\
			Glassfish & Server \\
			LaTeX & Typesetting (documentation only) \\
			SynergyNet & Multi-touch framework \\ 
			Android & Alternative framework \\ \hline
			Multi-touch tables & Hardware \\ \hline
		\end{tabularx}

	}

	\subsection{Project constraints} {
	
		
		Please refer to the Project Proposal document for more detail.

		\begin{itemize}
			\item Time
			\item Budget
			\item Technical Expertise
			\item Resources		
		\end{itemize}
		
	}

	\subsection{Assumptions} {

		For the planning of this project a number of assumptions have been made.
	
		\begin{description}
			\item[Team size] We assume that the size of the team shall remain constant; we currently have 13 members.
			\item[Delivery date] We assume that we have a hard deadline due to this project being conducted as a university assessment.
			\item [Team member availability] We assume that all members of the team will be able to allocate at least 10 hours per week to the project.
			\item[Tooling] We assume that we will have the ability to consistently access and make use of all operational tooling that we decide to use. This includes things such as our issue tracker and code repository.
			\item[Stakeholders] We assume that the core group of stakeholders shall remain constant.
			\item[Domain knowledge] We assume that the subject matter experts will be available within reasonable limits. We also assume that any user consumed content from the system will be provided by either the subject matter experts or other stakeholders.
		\end{description}

	}

	\subsection{Project funding} {

		There is no funding for this project.

	}

	\subsection{Project cost} {

		Almost all hardware and software the project requires are available at no cost, consequently budget issues are not of great concern to the project. However the following costs are relevant:
		
		\subsubsection{Domain name registration} {
			\textbf{\$13/year} - Cost for the domain name uonse.org.
		}
		
		\subsubsection{VPS server} {
			\textbf{\$5/month} - Cost of a VPS droplet hosted by DigitalOcean.
		}
		
		\subsubsection{Developer time} {
			\textbf{100 hours/week} - The main cost of this project is time, with 100/hours a week dedicated to the project by the entire team.
		}

	}

	\subsection{On-going costs post production} {

		There are two ongoing costs associated with the project:
		
		\begin{itemize}
			\item IT support, at a HEW 3 salary.
			\item Hardware replacement costs of any broken components.
		\end{itemize}
		
		Please see the project proposal for a more detailed listing of these costs.
	}

	\subsection{Project risk profile analysis} {

		A summary of the risk profile analysis conducted in the project proposal is outlined below:
		
		\begin{tabularx}{\textwidth}{| Y | Y | Y |}
			\hline
			\textbf{Risk description} & \textbf{Relative risk} & \textbf{Strategy} \\ \hline
			
			The project is not satisfactorily completed by the deadline. &
			Low &
			Reduction
			\\ \hline
			
			The requirements of the project are misinterpreted or not fully communicated.
			&
			Very Low &
			Reduction
			\\ \hline
			
			Tasks are not distributed evenly amongst members.&
			Very Low &
			Reduction
			\\ \hline
			
			Team members with limited technical expertise slow the development of the project. &
			Very Low &
			Avoidance
			\\ \hline
			
			The server provided by the University becomes unavailable. &
			Medium &
			Acceptance
			\\ \hline
			
			Members of the project do not complete their assigned task. &
			Low &
			Reduction
			\\ \hline
			
			The framework learning curve slows development. &
			High &
			Reduction
			\\ \hline
			
			Hardware failure slows or prevents development. &
			Very Low &
			Reduction
			\\ \hline
			
			The scope of the project becomes too large. &
			Low &
			Reduction
			\\ \hline
			
			Part of the codebase is lost.&
			Very Low &
			Reduction
			\\ \hline
			
			Too many processes slow development &
			High &
			Reduction
			\\ \hline
			
			Code that breaks the system or otherwise does not work is committed to the repository. &
			Very Low &
			Reduction
			\\ \hline
			
			Processes are not followed.&
			Low &
			Reduction
			\\ \hline
			
			Discussions are lost or forgotten. &
			Very Low &
			Reduction
			\\ \hline
			
			The large groups size becomes unmanageable.&
			Very Low &
			Reduction
			\\ \hline
			
			The communication between members is unclear and not to the point. &
			Very Low &
			Reduction
			\\ \hline
			
		\end{tabularx}

	}

}

\end{document}
